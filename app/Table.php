<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Table extends Model
{
    
   protected $table='tables';
    
    use Sortable;

   protected $fillable = [
        'name', 'school', 'degree','cgpa'
    ];
    
    public $sortable = ['id', 'name', 'school', 'degree','cgpa','created_at', 'updated_at'];
    
}
