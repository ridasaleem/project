<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $tables = Table::sortable()->paginate(7);
        return view('tables',compact('tables'));
        
       //  $tables = DB::table('tables')->get();
        
       // return view('tables', ['tables' => $tables]);
    }
    
    
    
    
    
    
}
