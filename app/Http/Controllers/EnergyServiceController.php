<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EnergyServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    
    public function index()
    {
        return view('energyservice');
    }
    
    
}
