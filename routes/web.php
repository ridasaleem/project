<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    return view('dashboard')->middleware('verified');
});


Route::get('/tables', function () {
    return view('tables')->middleware('verified');
});


Route::get('/verify', function () {
    return view('auth.verify');
});


Route::get('/energyservice', function () {
    return view('energyservice')->middleware('verified');
});


Route::get('/lockscreen', function () {
    return view('lockscreen');
});

Route::get('/passwordlock', function () {
    return view('passwordlock');
});

Route::get('/confirm', function () {
    return view('auth.passwords.confirm');
});



Auth::routes(['verify' => true]);

Route::get('/dashboard', 'DashBoardController@index')->name('dashboard');
Route::get('/tables', 'TableController@index')->name('tables');
Route::get('/passwordlock', 'PasswordLockController@index')->name('passwordlock');
Route::get('/energyservice', 'EnergyServiceController@index')->name('energyservice');
