<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    
	@extends('layout.head')
    
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box lockscreen clearfix">
					<div class="content">
						<h1 class="sr-only">Klorofil - Free Bootstrap dashboard</h1>
						<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div>
						<div class="user text-center">
							<img src="assets/img/user-medium.png" class="img-circle" alt="Avatar">
							<h2 class="name"> {{ Auth::user()->name }}</h2>
                            <h3 class="name"> A verfication link has been sent to your email, Please verify your account.  </h3>
                            
						</div>

                          @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                        
                    
                        
                         <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                               
                             <span class="input-group-btn">    <button class="btn btn-primary btn-lg btn-block" type="submit" class="helper-text"> {{ __('click here to request a new link') }}</button></span>
                    </form>
                        
                        
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
