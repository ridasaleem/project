<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    
	@extends('layout.head')
    
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box lockscreen clearfix">
					<div class="content">
						<h1 class="sr-only">Klorofil - Free Bootstrap dashboard</h1>
						<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div>
						<div class="user text-center">
							<img src="assets/img/user-medium.png" class="img-circle" alt="Avatar">
							<h2 class="name"> {{ Auth::user()->name }}</h2>
                            <h3 class="name"> Congratulations! Your account has been verified, Please enter your password to continue to the website  </h3>
                            
						</div>

                        
                        
                        <form method="POST" action="{{ route('password.confirm') }}">
                            @csrf
                            
							<div class="input-group">
								<input type="password" class="form-control" placeholder="Enter your password ..." name="password" required autocomplete="current-password">
                                  
                                
								<span class="input-group-btn"><button type="submit" class="btn btn-primary"><i class="fa fa-arrow-right"></i></button></span>
                                
							</div>
                            
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                               
                              @if (Route::has('password.request'))
								<div class="bottom">
									<span class="helper-text"> <a href="{{ route('password.request') }}">Forgot password?</a></span>
								</div>
                                @endif
						</form>
                        
                        
                    
                        
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
