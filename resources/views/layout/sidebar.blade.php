<div id="sidebar-nav" class="sidebar">
            <div class="sidebar-scroll">
				<nav>
                    <ul class="nav">
						<li><a href="{{ asset('/')}}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-code"></i> <span>Employee</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-chart-bars"></i> <span>Reports</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Energy Services</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                            
                            
                            
							<div id="subPages" class="collapse ">
                                
                               
								<ul class="nav">

                                     @foreach(explode(',', Auth::user()->energyservice) as $es) 
                                    <li><a href="{{ asset('energyservice')}}" class="">{{$es}}</a></li>
                                    @endforeach
								</ul>
							</div>
						</li>
						<li><a href="{{ asset('tables')}}" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-text-format"></i> <span>To Do List</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-linearicons"></i> <span>Expenses</span></a></li>
					</ul>
                </nav>
			</div>
		</div>