<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    
	@extends('layout.head')
    
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="{{ asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo"></div>
								<p class="lead">Reset Password</p>
							</div>
							<form class="form-auth-small" method="POST" action="{{ route('password.update') }}">
                                 @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                                
                                
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input type="email" class="form-control" id="signin-email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                                    
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>
                                
                                
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" class="form-control" id="signin-password" name="password" required autocomplete="new-password" placeholder="Password">
                                    
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>
                                
                                <div class="form-group">
									<label for="signin-password" class="control-label sr-only">Confirm Password</label>
									<input type="password" class="form-control" id="signin-password" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                                    
								</div>
                                
                                
                                
				
								<button type="submit" class="btn btn-primary btn-lg btn-block">RESET PASSWORD</button>
								
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Welcome to our website</h1>
							<p>easy Dashboard for all</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
