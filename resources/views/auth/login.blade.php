<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    
	@extends('layout.head')
    
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div>
                                
                                
								<p class="lead">Login to your account</p>
							</div>
							<form class="form-auth-small" action="{{ route('login') }}" method="POST">
                                @csrf
                                
                                
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
                                    
                                    
									<input type="email" class="form-control" name="email" id="signin-email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
                                    
                                    
                                     @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>
                                
								<div class="form-group">
                                    
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" class="form-control" id="signin-password" placeholder="Password" name="password" required autocomplete="current-password">
                                    
                                    
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>
                                
                                
								<div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
										<span>Remember me</span>
									</label>
								</div>
                                
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                                
                                
                                @if (Route::has('password.request'))
								<div class="bottom">
									<span class="helper-text"><i class="fa fa-lock"></i> <a href="{{ route('password.request') }}">Forgot password?</a></span>
								</div>
                                @endif
                                
							</form>
                            
                            <span class="helper-text"> <a href="{{ route('register') }}">SignUp to our website</a></span>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Welcome to our website</h1>
							<p>easy Dashboard for all</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
