<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    
	@extends('layout.head')
    
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div>
								<p class="lead">Register your account</p>
							</div>
							<form class="form-auth-small" method="POST" action="{{ route('register') }}">
                                @csrf
                                
                                
                                <div class="form-group">
									<label for="signin-email" class="control-label sr-only">Name</label>
									<input type="text" class="form-control" id="signin-email" name="name"  placeholder="Name" value="{{ old('name') }}" required autocomplete="name" autofocus >
                                    
                                    
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>
                                
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input type="email" class="form-control" id="signin-email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                                    
                                    
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>
                                
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" class="form-control" id="signin-password" name="password" required autocomplete="new-password" placeholder="Password">
                                    
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    
								</div>

                                <div class="form-group">
									<label for="signin-password" class="control-label sr-only">Confirm Password</label>
									<input type="password" class="form-control" id="signin-password" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                                    
								</div>
                                         <label  class="fancy-checkbox">
										<span>Subscribe to your Energy Service
                                             </span>
									</label>                       

                               <div class="form-group clearfix">
                                  
									<label class="fancy-checkbox element-left">
										<input type="checkbox" name="energyservice[]" value ="Energy Service 1">
										<span>E1</span>
									</label>
                                   
                                   <label class="fancy-checkbox element-left">
										<input type="checkbox" name="energyservice[]" value ="Energy Service 2">
										<span>E2</span>
									</label>
                                   
                                   <label class="fancy-checkbox element-left">
										<input type="checkbox" name="energyservice[]" value ="Energy Service 3">
										<span>E3</span>
									</label>
								</div>
								
								<button type="submit" class="btn btn-primary btn-lg btn-block">REGISTER</button>
							
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Welcome to our website</h1>
							<p>easy Dashboard for all</p>
                            <br/>
                            	<div class="bottom">
									<p class="helper-text"><a  style="color:white;" href="{{ route('login') }}">Login to an existing account?</a></p>
								</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
