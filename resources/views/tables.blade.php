<!doctype html>
<html lang="en">

<head>
    
@extends('layout.head')
    
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		
        @extends('layout.nav')
        
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		
        @include('layout.sidebar')
        
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">Tables</h3>
				
					<div class="row">
						
						<div class="col-md-12">
							<!-- TABLE HOVER -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Employee Data</h3>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>@sortablelink('id')</th>
												<th>@sortablelink('name')</th>
												<th>@sortablelink('school')</th>
												<th>@sortablelink('degree')</th>
                                                <th>@sortablelink('cgpa')</th>
                                                <th>@sortablelink('updated_at')</th>
											</tr>
										</thead>
										<tbody>
                                            @if($tables->count())
                                            @foreach($tables as $key => $table)
											<tr>
												<td>{{ $table->id}}</td>
												<td>{{ $table->name}}</td>
												<td>{{ $table->school}}</td>
												<td>{{ $table->degree}}</td>
                                                <td>{{ $table->cgpa}}</td>
                                                <td>{{ $table->updated_at}}</td>
											</tr>
                                            @endforeach
                                            @endif
											
											
										</tbody>
									</table>
                                     {!! $tables->appends(\Request::except('page'))->render() !!}
								</div>
							</div>
							<!-- END TABLE HOVER -->
						</div>
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
	
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
    
    
    
    
	<script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
	<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{ asset('assets/scripts/klorofil-common.js')}}"></script>
    
</body>

</html>
